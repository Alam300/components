import React, {useState} from 'react';
import styled from 'styled-components';

import {Button} from './components/buttons'
import {Input} from './components/form-fields'
import { Table } from './components/tables';
import { CardContainer, Flex } from './components/containers';
import {CardTitle,CardBody,CardFooter} from './components/card'

import "./App.css"

const Card = styled(CardContainer)`
  width:300px;
  border-radius:4px;
  box-shadow:0 0 12px 4px #d3d3d3;
`;

const StyledButton = styled(Button)`
  width:200px;
  text-align:left;
  margin:0.5rem 0 0 0.5rem;
  padding:0.5rem;
  border:1px solid green;
  border-radius:0.25rem;
`;

function App() {
  const [name,setName] = useState('HI');
  const [loading,setLoading] = useState(false);

  const onNameChange=(e)=>{
    const {target} = e;
    setName(target.value)
  }

  const changeLoading=()=>{
    setLoading(true);
    setName('HI');
    setTimeout(()=>{setLoading(false)},2000);
  }

  return (
    <Flex>
      <Flex direction="row">
        {[1,2,3].map((card,idx)=>(
            <Card 
              key={'card'+card} 
              bgColor={idx===0?"red":"white"} 
              mt={2} 
              ml={2}
            >
              <CardTitle>
                <StyledButton bgColor="green">
                  Schedule Test
                </StyledButton>
              </CardTitle>
              <CardBody>
                <Flex>
                  <h3>Hey</h3>
                  <p>this is all about Schedule tests</p>
                </Flex>
              </CardBody>
              <CardFooter>footer</CardFooter>  
            </Card>
          )
        )}
      </Flex>
      <Flex mt={2}>
        <Input
          placeholder="Hey Buddy"
          value={name} 
          onChange={onNameChange}
        />
        <p>{name}</p>
        <Button 
          loading={loading} 
          onClick={changeLoading}
        >
          click me
        </Button>
        <Table/>
      </Flex>
    </Flex>
  );
}

export default App;
