import React from 'react'
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledButton=styled.button`
    background-color:${props=>props.disabled?'grey':props.bgColor?props.bgColor:'blue'};
    outline:0;
    color:${props=>props.disabled?'white':props.color?props.color:'white'};
    margin-top:${props=>props.mt?props.mt+"rem":'0'};
    margin-bottom:${props=>props.mb?props.mb+"rem":'0'};
    margin-left:${props=>props.ml?props.ml+"rem":'0'};
    margin-right:${props=>props.mr?props.mr+"rem":'0'};
    padding-top:${props=>props.pt?props.pt+"rem":'0'};
    padding-bottom:${props=>props.pb?props.pb+"rem":'0'};
    padding-left:${props=>props.pl?props.pl+"rem":'0'};
    padding-right:${props=>props.pr?props.pr+"rem":'0'};
`

const Button = ({children, loading, onClick, ...props}) => (
    <StyledButton 
        disabled={loading} 
        onClick={onClick} 
        data-testid="basic-button"
        {...props}
    >
        {children}
    </StyledButton>
)

Button.propTypes={
    loading:PropTypes.bool,
    onClick:PropTypes.func
}

Button.defaultProps={
    loading:false
}

export default Button