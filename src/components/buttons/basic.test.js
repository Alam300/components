import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import Button from './basic';

const defaultProps={
    loading:false,
    onClick:jest.fn()
};

const buttonText="click me";

describe('Basic button tests',()=>{
    it("should have click me text",()=>{
        const {getByText, rerender, debug} = render(
            <Button {...defaultProps}>
                {buttonText}
            </Button>
        );
        expect(getByText(buttonText)).toBeTruthy();

        rerender(<Button {...defaultProps}>{"send"}</Button>)
        expect(getByText('send')).toBeTruthy();
    })

    it("should disable before click me button",()=>{
        const {getByTestId} = render(
            <Button {...defaultProps}>
                {buttonText}
            </Button>);

        expect(getByTestId(/basic-button/i).disabled).toBeFalsy();
    })

    it("should disable after click me button",()=>{
        const {getByTestId} = render(
            <Button 
                {...defaultProps}
                loading={true}
            >
                {buttonText}
            </Button>);

        fireEvent.click(getByTestId(/basic-button/i));
        expect(getByTestId(/basic-button/i).disabled).toBeTruthy();
    })
})