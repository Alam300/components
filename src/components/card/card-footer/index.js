import React from 'react'

import { Flex } from '../../containers'

const CardFooter=({children,...props})=>{
    return(
        <Flex {...props}>
            {children}
        </Flex>
    )
}

export default CardFooter;