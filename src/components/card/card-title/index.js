import React from 'react'

import { Flex } from '../../containers'

const CardTitle=({children,...props})=>{
    return(
        <Flex {...props}>
            {children}
        </Flex>
    )
}

export default CardTitle;