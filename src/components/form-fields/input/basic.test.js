import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import Input from './basic';

const onChange=jest.fn();

describe("Testing Basic input",()=>{
    it("should be placeholder as ''",()=>{
        const placeholder="";

        const {getByTestId}=render(
            <Input onChange={onChange}/>
        )
        const inputNode=getByTestId("basic-input");
        expect(inputNode.placeholder).toBe(placeholder);
    })

    it("should be initial value as ''",()=>{
        const value="";

        const {getByTestId}=render(
            <Input onChange={onChange}/>
        )
        const inputNode=getByTestId("basic-input");
        expect(inputNode.value).toBe(value);
    })

    it("should be type as 'text'",()=>{
        let type="text";
        const {getByTestId, rerender}=render(
            <Input type={type} onChange={onChange}/>
        )
        expect(getByTestId("basic-input").type).toBe(type);

        type="password";
        rerender(
            <Input type={type} onChange={onChange}/>
        )
        expect(getByTestId("basic-input").type).toBe(type);
    })

    it("should change the value of the input",()=>{
        const value="alam basha"

        const {getByTestId}=render(
            <Input value={value} onChange={onChange}/>
        )
        fireEvent.change(getByTestId(/basic-input/i), {target: {value: value}})

        expect(getByTestId(/basic-input/i).value).toBe(value);
    })
})