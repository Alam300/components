import React from 'react';
import styled from 'styled-components';

const StyledTr=styled.tr`
    padding: 0 8px;
`;
const StyledTd=styled.td`
    padding: 0 8px;
`;
const StyledTh=styled.th`
    padding: 0 8px;
    align-items:left;
`;

const Table =()=>{
    return(
        <table>
            <StyledTr>
                <StyledTh>SNo</StyledTh>
                <StyledTh>Name</StyledTh>
                <StyledTh>Class</StyledTh>
                <StyledTh>Marks</StyledTh>
            </StyledTr>
            <StyledTr>
                <StyledTd>1</StyledTd>
                <StyledTd>alam</StyledTd>
                <StyledTd>x</StyledTd>
                <StyledTd>90</StyledTd>
            </StyledTr>
            <StyledTr>
                <StyledTd>2</StyledTd>
                <StyledTd>basha</StyledTd>
                <StyledTd>xii</StyledTd>
                <StyledTd>7.5</StyledTd>
            </StyledTr>
            <StyledTr>
                <StyledTd>3</StyledTd>
                <StyledTd>kundan</StyledTd>
                <StyledTd>xvi</StyledTd>
                <StyledTd>7.5</StyledTd>
            </StyledTr>
        </table>
    )
}

export default Table