//class based comp
import React, {Component} from 'react'

class a extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <div></div>
        );
    }
}

export default a;

//func with state
import React,{useState} from 'react';

const a = () => {
    return(
        <div></div>
    );
}

//func
export default a;

import React from 'react';

const a=()=> <div></div>

export default a;

//map state to props
const mapStateToProps=(state)=>{
    return {

    }
}

//mapDispatchToProps
function mapDispatchToProps(dispatch){
    return{

    }
}

//reducer
import {} from './types';

const INIT_STATE={}

const a=(state=INIT_STATE, action)=>{
    switch(action.type){
        case "":{

        }
        default:{return state}
    }
}
export a;

//action creator
import {} from './types';

export const a=()=>{
    return {
        type:"",
        payload:""
    }
}

//saga
import {call, put, takeLatest} from 'redux-saga/effects'
import {} from './types';
import {} from './actions';

function* a(action){}

export default function* (){
    yield takeLatest("",a);
}
